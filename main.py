
from __future__ import absolute_import, unicode_literals, print_function

from foo.hello import func as f1
from bar.hello import func as f2
from bar.baz.hello import func as f3

f1()
f2()
f3()
